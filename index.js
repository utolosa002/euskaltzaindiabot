"use strict";

const { TelegramBaseController, TextCommand } = require("telegram-node-bot");
const Bot = require("./helpers/botConnection");
const tg = Bot.get();
const InlineMode = require("./inlineMode");

/**
 *  Controllers
 */
class kaixoController extends TelegramBaseController {
  /**
   * @param {Scope} $
   */
  kaixoHandler($) {
    $.sendMessage("Idatzi: @Etzaindia hitza");
  }

  get routes() {
    return {
      kaixoCommand: "kaixoHandler",
    };
  }
}

/**
 * Routers
 */

tg.router.when(
  new TextCommand("/kaixo", "kaixoCommand"),
  new kaixoController()
);
tg.router.inlineQuery(new InlineMode());
