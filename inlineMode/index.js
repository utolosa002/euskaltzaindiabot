const { TelegramBaseInlineQueryController } = require("telegram-node-bot");
var HTMLParser = require("node-html-parser");
const axios = require("axios");

const Bot = require("../helpers/botConnection");
const bot = Bot.get();

class InlineMode extends TelegramBaseInlineQueryController {
  /**
   * Reply inline queries
   * @param {InlineScoper} $
   */
  async handle($) {
    const { id, from, query } = $.inlineQuery;
    const userName = from.firstName;
    const userId = from.id;

    if (query) {
      const msg = query;
      const URL = `https://www.euskaltzaindia.eus/index.php?sarrera2=${msg}&antzekoak=ez&option=com_hiztegianbilatu&view=frontpage&layout=aurreratua&lang=eu&bila=bai`;
      ///////////////

      axios
        .get(URL)
        .then(function (response) {
          // handle success

          //console.log("lol"+response.data);
          var orria = HTMLParser.parse(response.data);
          var bilaketak = orria.querySelector("#searchContent");
          var hitzaString = "";

          // console.log("luz: "+ bilaketak.querySelector('#searchResult'));
          var bilaketa = bilaketak.querySelectorAll(".searchResult");
          var hitz_zerrenda = [];
          bilaketa
            .slice(-8)
            .forEach((hitza) =>
              hitz_zerrenda.push(hitza.querySelector("h3").text)
            );

          console.log(`hitz_zerrenda.length: ${hitz_zerrenda.length}`);
          console.log("bilaketa.length: " + bilaketa.length);

          var result = [];
          var adierak = [];
          var adieraString = "";
          for (var i = 0; i < bilaketa.length && i < 8; i++) {
            hitzaString = bilaketa[i].querySelector("h3").text;

            // adierak = bilaketa[i].querySelectorAll(".adiera, .azpisarrera");

            // adieraString = "";
            // adierak.forEach(
            //   (adiera) => (adieraString = adieraString + "\n" + adiera.text)
            // );
            result[i] = {
              type: "article",
              id: i,
              title: hitzaString,
              input_message_content: {
                message_text: (
                  `*${hitzaString}*` +
                  "\n" +
                  bilaketa[i].querySelector(".lehenlerroa").text +
                  "\n" +
                  //adieraString+
                  `[Informazio gehiago...](${URL})`
                ).substring(0, 4000),
                parse_mode: "Markdown",
              },
              reply_markup: {},
            };

            // JSON.stringify(result_list) + JSON.stringify(result);
            console.log("sresult: " + result[i]);
          }

          const inineQueryAnswered = bot.api.answerInlineQuery(id, result);
        })
        .catch(function (error) {
          // handle error
          console.log(`errorea!:${error}`);
        });
    }
  }
}

module.exports = InlineMode;
